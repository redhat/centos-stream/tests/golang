#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/golang/Sanity/tls13-obsolete-curves
#   Description: Verify that obsolete curves cannot be negotiated in TLS 1.3
#   Author: Alexander Sosedkin <asosedki@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="golang"

LOOKASIDE=${LOOKASIDE:-http://download.eng.bos.redhat.com/qa/rhts/lookaside/}
TLSLITE_LOOKASIDEDIR="/tlslite/"
TLSFUZZER="tlsfuzzer-0.0.0-alpha65.tar.gz"
GOLANG_LOOKASIDEDIR="/golang/"
GOTLSDRIVER="go-tlsdriver-0.0.1.tar.gz"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "rlImport openssl/certgen"
        rlRun "rlImport distribution/fips"
        [[ -r  $POLICY_FILE ]] && rlRun "rlFileBackup --clean $POLICY_FILE"
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "x509KeyGen localhost"
        rlRun "x509SelfSign localhost"
        rlRun "wget $LOOKASIDE/$TLSLITE_LOOKASIDEDIR/$TLSFUZZER"
        rlRun "tar xf $TLSFUZZER"
        rlRun "wget $LOOKASIDE/$GOLANG_LOOKASIDEDIR/$GOTLSDRIVER"
        rlRun "tar xf $GOTLSDRIVER"
	rlRun "pushd go-tlsdriver/cmd/server"
	rlRun "go install"
        rlRun "popd"
        FIPS_CURVES="CurveP256 CurveP384 CurveP521"
        NONFIPS_CURVES="X25519"
	TLSFUZZER_OBSOLETE_CURVES="\
brainpoolP256r1
brainpoolP384r1
brainpoolP512r1
secp160k1
secp160r1
secp160r2
secp192k1
secp192r1
secp224k1
secp224r1
secp256k1
sect163k1
sect163r1
sect163r2
sect193r1
sect193r2
sect233k1
sect233r1
sect239k1
sect283k1
sect283r1
sect409k1
sect409r1
sect571k1
sect571r1
unknown (65281)
unknown (65282)
"
    rlPhaseEnd

    rlPhaseStartTest "Test that server rejects obsolete curves"
        srv_params=()
        srv_params+=("--keyfile $(x509Key localhost)")
        srv_params+=("--certfile $(x509Cert localhost)")
        srv_params+=("--http")
        srv_params+=("--address :4433")
        if ! fipsIsEnabled || rlIsRHEL '<9' || rlIsFedora '<36'; then
	    srv_curves="${NONFIPS_CURVES} ${FIPS_CURVES}"
        else
            srv_curves="${FIPS_CURVES}"
        fi
	for srv_curve in $srv_curves; do
            srv_params+=("--curve ${srv_curve}")
	done
        rlRun "$HOME/go/bin/server ${srv_params[*]} &>server.log &"
        srv_pid=$!
        rlRun "rlWaitForSocket -t 5 4433"
        expect_fail=(
	    # x448 is not supported at all
	    "-x 'x448 in supported_groups and key_share' -X 'handshake_failure'")
        if fipsIsEnabled; then
            expect_fail+=("-x 'x25519 in supported_groups and key_share' -X 'handshake_failure'")
        fi
	nl='
'
	save_IFS="$IFS"
	IFS="$nl"
	for obsolete_curve in $TLSFUZZER_OBSOLETE_CURVES; do
            expect_fail+=("-x '$obsolete_curve in key_share and secp256r1 in supported_groups (inconsistent extensions)'")
	done
	IFS="$save_IFS"
        rlRun "PYTHONPATH=tlsfuzzer python3 tlsfuzzer/scripts/test-tls13-obsolete-curves.py -a handshake_failure --relaxed ${expect_fail[*]}"
        rlRun "kill $srv_pid" 0,1
        rlRun "rlWait -t 5 $srv_pid" 143,0
        if ! rlGetPhaseState; then
            rlRun "cat server.log"
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
